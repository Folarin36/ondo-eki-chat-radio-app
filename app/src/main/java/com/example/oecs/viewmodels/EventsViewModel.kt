package com.example.oecs.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.oecs.models.EventsResponse
import com.example.oecs.utils.Result
import com.example.oecs.repository.OecsRepositoryImpl

class EventsViewModel(private val repositoryImpl: OecsRepositoryImpl) : ViewModel() {
    private var _eventsLocal = MutableLiveData<Result<List<EventsResponse>>>()
    val events: LiveData<Result<List<EventsResponse>>>
        get() = _eventsLocal

    fun eventsRemote() {
        _eventsLocal.postValue(Result.Loading("Loading..."))
        repositoryImpl.getEventsRemote(
            { list ->
                _eventsLocal.postValue(Result.Success(list!!))
            }, { localizedMessage ->
                _eventsLocal.postValue(Result.Error(localizedMessage))
            })
    }

}







