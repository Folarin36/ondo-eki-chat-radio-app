package com.example.oecs.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.oecs.models.NewsResponse
import com.example.oecs.models.NewsResponseItem
import com.example.oecs.repository.OecsRepositoryImpl
import com.example.oecs.utils.Result

class NewsViewModel(private val repositoryImpl: OecsRepositoryImpl) : ViewModel() {
    private var _newsLocal = MutableLiveData<Result<List<NewsResponseItem>>>()
    val news: LiveData<Result<List<NewsResponseItem>>>
        get() = _newsLocal

    fun newsFlashRemote() {
        _newsLocal.postValue(Result.Loading("Loading..."))
        repositoryImpl.getNewsFlashRemote(
            { list ->
                _newsLocal.postValue(Result.Success(list!!))
            }, { localizedMessage ->
                _newsLocal.postValue(Result.Error(localizedMessage))
            })
    }

}