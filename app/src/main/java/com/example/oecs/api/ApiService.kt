package com.example.oecs.api

import androidx.viewbinding.BuildConfig
import com.example.oecs.utils.AppConstants.BASE_URL
import com.example.oecs.utils.AppConstants.CALL_TIMEOUT
import com.example.oecs.utils.AppConstants.WRITE_TIMEOUT
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun gson(): Gson {
    return GsonBuilder().setLenient().serializeNulls().create()
}

fun okHttpClient(): OkHttpClient {
    val clientBuilder = OkHttpClient.Builder().apply {
        callTimeout(CALL_TIMEOUT, TimeUnit.SECONDS)
        writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor =
                HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT).apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            addInterceptor(httpLoggingInterceptor)
        }
    }
    return clientBuilder.build()
}

fun buildRetrofit(baseUrl: String): Retrofit {
    return Retrofit.Builder()
        .client(okHttpClient())
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson()))
        .build()
}

fun buildAPIService(): ApiClient = buildRetrofit(BASE_URL).create(ApiClient::class.java)

