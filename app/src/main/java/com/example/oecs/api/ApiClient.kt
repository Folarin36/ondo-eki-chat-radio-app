package com.example.oecs.api

import com.example.oecs.models.EventsResponse
import com.example.oecs.models.NewsResponse
import com.example.oecs.models.NewsResponseItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiClient {

    @GET("event/read.php")
    fun getEvents(): Call<List<EventsResponse>>

    @GET("news/read.php")
    fun getNewsFlash(): Call<List<NewsResponseItem>>
}