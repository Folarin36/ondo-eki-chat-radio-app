package com.example.oecs.onboarding

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.example.oecs.HomeActivity
import com.example.oecs.R
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator

class OnboardingScreen : AppCompatActivity() {
    private lateinit var viewPager: ViewPager
    private lateinit var onboardingAdapter: OnboardingAdapter
    private lateinit var indicatorsContainer: LinearLayout
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding_screen)
        button = findViewById(R.id.next_button)
        setOnboardingItems()
        setUpIndicators()
        setCurrentIndicator(0)
    }

    @SuppressLint("SetTextI18n")
    private fun setOnboardingItems() {
        onboardingAdapter = OnboardingAdapter(
            listOf(
                OnboardingItem(
                    title = "Enjoy\nRadio & TV\non the Go!!!"
                ),
                OnboardingItem(
                    title = "Plus Podcasts,\nNews, Events & More..."
                ),
                OnboardingItem(
                    title = "Have Fun \nWith Basic Quiz Questions..."
                )
            )
        )
        val onboardingViewPager = findViewById<ViewPager2>(R.id.view_pager)
        onboardingViewPager.adapter = onboardingAdapter
        onboardingViewPager.registerOnPageChangeCallback(object:
        ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentIndicator(position)
            }
        })
        (onboardingViewPager.getChildAt(0) as RecyclerView).overScrollMode =
            RecyclerView.OVER_SCROLL_NEVER
        findViewById<Button>(R.id.next_button).setOnClickListener {
            if ( onboardingViewPager.currentItem + 1 < onboardingAdapter.itemCount){
                onboardingViewPager.currentItem += 1
            } //else if( onboardingViewPager.currentItem +1 < onboardingAdapter.itemCount){
                //button.setText(R.string.finish)
                //navigateToHomeActivity()
            //}
            else {
                onboardingViewPager.currentItem += 2
                button.setText(R.string.finish)
                navigateToHomeActivity()
                onboardingFinished()
            }
        }
        findViewById<TextView>(R.id.skip_text).setOnClickListener {
            navigateToHomeActivity()
        }

    }

    private fun navigateToHomeActivity(){
        startActivity(Intent(applicationContext, HomeActivity::class.java))
        finish()
    }

    private fun setUpIndicators() {
        indicatorsContainer = findViewById(R.id.onboardingIndicators)
        val indicators = arrayOfNulls<ImageView>(onboardingAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8, 0, 8, 0)
        for ( i in indicators.indices){
            indicators[i] = ImageView(applicationContext)
            indicators[i]?.let {
                it.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_active
                    )
                )
                it.layoutParams = layoutParams
                indicatorsContainer.addView(it)
            }
        }
    }
// current indicator for onboarding screen
    private fun setCurrentIndicator(position: Int){
        val childCount = indicatorsContainer.childCount
        for (i in 0 until childCount){
            val imageView = indicatorsContainer.getChildAt(i) as ImageView
            if ( i == position){
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_active
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )
            }
        }

    }
     // shared preferences to show onboarding screens for first time users
    private fun onboardingFinished(){
        val sharedPref = getSharedPreferences("onboarding", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean("Finish", true)
        editor.apply()

    }
}