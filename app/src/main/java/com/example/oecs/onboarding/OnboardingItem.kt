package com.example.oecs.onboarding

data class OnboardingItem(
    val title: String
)
