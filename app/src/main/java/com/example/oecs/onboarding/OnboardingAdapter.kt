package com.example.oecs.onboarding

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.example.oecs.R

class OnboardingAdapter(private val onboardingItems: List<OnboardingItem>):
RecyclerView.Adapter<OnboardingAdapter.OnboardingItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnboardingItemViewHolder {
    return OnboardingItemViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.onboarding_list_item,
            parent,
            false
        )
    )
    }

    override fun onBindViewHolder(holder: OnboardingItemViewHolder, position: Int) {
     holder.bind(onboardingItems[position])
    }

    override fun getItemCount(): Int {
      return onboardingItems.size
    }

    inner class OnboardingItemViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val textTitle = view.findViewById<TextView>(R.id.text_title)

        fun bind(onboardingItem: OnboardingItem){
            textTitle.text = onboardingItem.title
        }
    }
}
