package com.example.oecs.repository

import com.example.oecs.models.EventsResponse
import com.example.oecs.api.ApiClient
import com.example.oecs.models.NewsResponse
import com.example.oecs.models.NewsResponseItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * created by Isaac Folarin
 */

class OecsRepositoryImpl (private val api: ApiClient):
    OecsRemoteRepository {
    override fun getEventsRemote(
        onSuccess: (List<EventsResponse>?) -> Unit,
        onFailure: (String) -> Unit
    ) {
        api.getEvents().enqueue(object : Callback<List<EventsResponse>?> {
            override fun onResponse(
                call: Call<List<EventsResponse>?>,
                response: Response<List<EventsResponse>?>
            ) {
                onSuccess(response.body())
            }

            override fun onFailure(call: Call<List<EventsResponse>?>, t: Throwable) {
                onFailure(t.localizedMessage!!)
            }
        })
    }

    override fun getNewsFlashRemote(
        onSuccess: (List<NewsResponseItem>?) -> Unit,
        onFailure: (String) -> Unit
    ) {
        api.getNewsFlash().enqueue(object : Callback<List<NewsResponseItem>?> {
            override fun onResponse(
                call: Call<List<NewsResponseItem>?>,
                response: Response<List<NewsResponseItem>?>
            ) {
                onSuccess(response.body())
            }

            override fun onFailure(call: Call<List<NewsResponseItem>?>, t: Throwable) {
                onFailure(t.localizedMessage!!)
            }
        })


    }


}



