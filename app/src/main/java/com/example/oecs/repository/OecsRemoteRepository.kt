package com.example.oecs.repository

import com.example.oecs.models.EventsResponse
import com.example.oecs.models.NewsResponse
import com.example.oecs.models.NewsResponseItem

/**
 * created by Isaac Folarin
 */

interface OecsRemoteRepository {

    fun getEventsRemote(
        onSuccess: (List<EventsResponse>?) -> Unit,
        onFailure: (String) -> Unit
    )

    fun getNewsFlashRemote(
        onSuccess: (List<NewsResponseItem>?) -> Unit,
        onFailure: (String) -> Unit
    )

}