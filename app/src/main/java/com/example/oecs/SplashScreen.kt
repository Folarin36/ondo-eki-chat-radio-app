package com.example.oecs

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils.loadAnimation
import android.widget.ImageView
import com.example.oecs.onboarding.OnboardingScreen

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val logo = findViewById<ImageView>(R.id.splash_image)

        logo.startAnimation(loadAnimation(this, R.anim.anim_fade_in))
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            if(onboardingFinished()){
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()

            } else {
                val intent = Intent(this, OnboardingScreen::class.java)
                startActivity(intent)
                finish()
            }

        }, 5000)

    }

    private fun onboardingFinished(): Boolean{
        val sharedPref = getSharedPreferences("onboarding", Context.MODE_PRIVATE)
        return sharedPref.getBoolean("Finish", false)
    }

}
