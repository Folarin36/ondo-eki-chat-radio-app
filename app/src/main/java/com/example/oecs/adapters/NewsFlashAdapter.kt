package com.example.oecs.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.oecs.R
import com.example.oecs.databinding.NewsFlashListItemsBinding
import com.example.oecs.models.NewsResponse
import com.example.oecs.models.NewsResponseItem
import java.util.ArrayList

class NewsFlashAdapter:
    RecyclerView.Adapter<NewsFlashAdapter.NewsFlashRecyclerViewHolder>() {

    private var values: List<NewsResponseItem> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NewsFlashRecyclerViewHolder{
        return NewsFlashRecyclerViewHolder(
            NewsFlashListItemsBinding.bind(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.news_flash_list_items, parent, false)
            )
        )
    }

    override fun getItemCount() = values.size

    override fun onBindViewHolder(holder: NewsFlashRecyclerViewHolder, position: Int) =
        holder.bind(values[position])

    fun setData(values: List<NewsResponseItem>) {
        this.values = values
        notifyDataSetChanged()
    }

    class NewsFlashRecyclerViewHolder(private val binding: NewsFlashListItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NewsResponseItem) = with(binding) {
            binding.textHead.text = item.title
            binding.day.text = item.data
            binding.time.text = item.time
            binding.fullText.text = item.body

        }
    }
}