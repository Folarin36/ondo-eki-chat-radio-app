package com.example.oecs.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.oecs.R
import com.example.oecs.databinding.EventsListItemsBinding
import com.example.oecs.models.EventsResponse
import java.util.ArrayList

class EventsAdapter:
    RecyclerView.Adapter<EventsAdapter.EventsRecyclerViewHolder>() {

    private var data: List<EventsResponse> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventsRecyclerViewHolder{
        return EventsRecyclerViewHolder(
            EventsListItemsBinding.bind(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.events_list_items, parent, false)
            )
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: EventsRecyclerViewHolder, position: Int) =
        holder.bind(data[position])

    fun setData(data: List<EventsResponse>) {
        this.data = data
        notifyDataSetChanged()
    }

    class EventsRecyclerViewHolder(private val binding: EventsListItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: EventsResponse) = with(binding) {
            binding.eventsTitle.text = item.title
            binding.organizer.text = item.organizers
            binding.venue.text = item.venue
            binding.date.text = item.date
            binding.eventsTime.text = item.time
        }
    }
}


