package com.example.oecs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.oecs.R

class ListenNowFragment: Fragment(){
    private lateinit var podcastsCard: CardView
    private lateinit var eventsCard: CardView
    private lateinit var weatherCard: CardView
    private lateinit var newsCard: CardView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_listen_now, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        podcastsCard = view.findViewById(R.id.podcasts_card)
        eventsCard = view.findViewById(R.id.events_card)
        weatherCard = view.findViewById(R.id.weather_card)
        newsCard = view.findViewById(R.id.news_card)

        podcastsCard.setOnClickListener {
            view.findNavController().navigate(R.id.podcastsFragment)
        }
        eventsCard.setOnClickListener {
            view.findNavController().navigate(R.id.eventsFragment)
        }
        weatherCard.setOnClickListener {
            view.findNavController().navigate(R.id.weatherFragment)
        }
        newsCard.setOnClickListener {
            view.findNavController().navigate(R.id.newsFlashFragment)
        }
    }
}