package com.example.oecs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.oecs.App
import com.example.oecs.adapters.NewsFlashAdapter
import com.example.oecs.databinding.FragmentNewsFlashBinding
import com.example.oecs.models.NewsResponse
import com.example.oecs.models.NewsResponseItem
import com.example.oecs.utils.Result
import com.example.oecs.utils.displaySnackBar
import com.example.oecs.utils.Result.Success
import com.example.oecs.utils.Result.Error
import com.example.oecs.utils.Result.Loading
import com.example.oecs.utils.hide
import com.example.oecs.utils.show
import com.example.oecs.viewmodels.NewsViewModel
import java.util.ArrayList

class NewsFlashFragment: Fragment() {
    private lateinit var binding: FragmentNewsFlashBinding
    private var newsFlashAdapter = NewsFlashAdapter()
    private val viewModel: NewsViewModel by lazy {
        ViewModelProvider(
            requireActivity(),
            App.newsViewModelFactory
        )[NewsViewModel::class.java]

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewsFlashBinding.inflate(layoutInflater)

        binding.newsRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
        }


        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadRemoteData()
        viewModel.news.observe(viewLifecycleOwner, Observer { result->
            when (result) {
                is Success -> {
                    newsFlashAdapter.setData(result.data)
                    binding.newsRecyclerView.adapter = newsFlashAdapter
                    binding.loadingAnimation.hide()
                    binding.noNetworkAnimation.hide()

                }
                is Error -> {
                    binding.root.displaySnackBar(result.error)
                    binding.noNetworkAnimation.show()
                    binding.loadingAnimation.hide()

                }
                is Loading -> {
                    binding.loadingAnimation.show()
                    binding.noNetworkAnimation.hide()
                }
            }
        })
    }
    private fun loadRemoteData() {
        viewModel.newsFlashRemote()
    }

}