package com.example.oecs.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import com.example.oecs.R

class SocialConnectFragment: Fragment(){
    private lateinit var facebookLayout: RelativeLayout
    private lateinit var twitterLayout: RelativeLayout
    private lateinit var instagramLayout: RelativeLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_social_connect, container, false)
        facebookLayout = view.findViewById(R.id.facebook_layout)
        twitterLayout = view.findViewById(R.id.twitter_layout)
        instagramLayout = view.findViewById(R.id.instagram_layout)

        // This opens the social media pages when each button is clicked

        facebookLayout.setOnClickListener {
            val url = "https://www.facebook.com/oecsrd"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

        twitterLayout.setOnClickListener {
            val url = "https://twitter.com/OecsTv"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)

        }

        instagramLayout.setOnClickListener {
            val url = "https://www.instagram.com/oecsonlineradioandtv/"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)

        }

        return view
    }

}