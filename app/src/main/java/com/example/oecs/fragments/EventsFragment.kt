package com.example.oecs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.oecs.App
import com.example.oecs.R
import com.example.oecs.adapters.EventsAdapter
import com.example.oecs.databinding.EventsListItemsBinding
import com.example.oecs.databinding.FragmentEventsBinding
import com.example.oecs.utils.Result
import com.example.oecs.utils.Result.Success
import com.example.oecs.utils.Result.Error
import com.example.oecs.utils.Result.Loading
import com.example.oecs.utils.displaySnackBar
import com.example.oecs.utils.hide
import com.example.oecs.utils.show
import com.example.oecs.viewmodels.EventsViewModel


class EventsFragment : Fragment() {
    private lateinit var binding: FragmentEventsBinding
    private var eventsAdapter = EventsAdapter()
    private val viewModel: EventsViewModel by lazy {
        ViewModelProvider(
            requireActivity(),
            App.viewModelFactory
        )[EventsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEventsBinding.inflate(layoutInflater)


        binding.eventsRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
        }
        return binding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadRemoteData()
        viewModel.events.observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Success -> {
                    eventsAdapter.setData(result.data)
                    binding.eventsRecyclerView.adapter = eventsAdapter
                    binding.loadingAnimation.hide()
                    binding.noNetworkAnimation.hide()

                }
                is Error -> {
                    binding.root.displaySnackBar(result.error)
                    binding.noNetworkAnimation.show()
                    binding.loadingAnimation.hide()

                }
                is Loading -> {
                    binding.loadingAnimation.show()
                    binding.noNetworkAnimation.hide()
                }
            }
        })
    }

    private fun loadRemoteData() {
        viewModel.eventsRemote()
    }
}

