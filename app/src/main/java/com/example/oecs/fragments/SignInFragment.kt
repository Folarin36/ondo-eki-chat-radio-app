package com.example.oecs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.oecs.R

class SignInFragment: Fragment() {
    private lateinit var button: Button
    private lateinit var beginButton: Button
    private lateinit var mView: View
    private lateinit var editEmail: EditText
    private lateinit var editPassword: EditText


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_sign_in, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button = mView.findViewById(R.id.register_button)
        beginButton = mView.findViewById(R.id.begin_button)
        editEmail = mView.findViewById(R.id.edit_email)
        editPassword = mView.findViewById(R.id.edit_password)

        button.setOnClickListener {
            view.findNavController().navigate(R.id.signUpFragment)
        }
        beginButton.setOnClickListener {
            view.findNavController().navigate(R.id.beginTriviaFragment)
        }


    }
}