package com.example.oecs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.oecs.databinding.FragmentFullNewsBinding

class FullNewsFragment: Fragment() {
    private lateinit var binding: FragmentFullNewsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFullNewsBinding.inflate(layoutInflater)

        return binding.root
    }
}