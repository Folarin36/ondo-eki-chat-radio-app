package com.example.oecs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.oecs.R

class SignUpFragment: Fragment() {
    private lateinit var mView: View
    private lateinit var beginButton: Button
    private lateinit var signInButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_sign_up, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        beginButton = mView.findViewById(R.id.begin_button)
        signInButton = mView.findViewById(R.id.sign_in_button)

        beginButton.setOnClickListener {
            view.findNavController().navigate(R.id.beginTriviaFragment)
        }
        signInButton.setOnClickListener {
            view.findNavController().navigate(R.id.signInFragment)
        }
    }
}