package com.example.oecs

import android.app.Application
import com.example.oecs.api.ApiClient
import com.example.oecs.api.buildAPIService
import com.example.oecs.repository.OecsRepositoryImpl
import com.example.oecs.viewmodels.EventsViewModelFactory
import com.example.oecs.viewmodels.NewsViewModelFactory

class App: Application() {
    companion object {
        private val service: ApiClient
            get() = buildAPIService()

        private val oecsRepositoryImpl: OecsRepositoryImpl
            get() = OecsRepositoryImpl(service)

        val viewModelFactory: EventsViewModelFactory
            get() = EventsViewModelFactory(
                oecsRepositoryImpl
            )
        val newsViewModelFactory: NewsViewModelFactory
         get() = NewsViewModelFactory(
             oecsRepositoryImpl
         )
    }
}