package com.example.oecs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import com.example.oecs.databinding.ActivityHomeBinding
import com.example.oecs.viewmodels.NewsViewModel
import com.example.oecs.viewmodels.NewsViewModelFactory
import com.google.android.material.navigation.NavigationView

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private lateinit var navigationView: NavigationView
    private lateinit var binding: ActivityHomeBinding
    lateinit var viewModel: NewsViewModel
    private lateinit var textView: TextView
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        toolbar = findViewById(R.id.toolbar)
        navigationView = findViewById(R.id.navigation_view)
        drawerLayout = findViewById(R.id.drawer_layout)

        setSupportActionBar(toolbar)
        setupViews()

        navController = Navigation.findNavController(this, R.id.host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration)

    }
    private fun setupViews() {
        navigationView.setNavigationItemSelectedListener(this)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.listenNowFragment -> {
                navController.popBackStack(R.id.listenNowFragment, false)
            }
            R.id.podcastsFragment -> {
                navController.navigate(R.id.podcastsFragment)
            }
            R.id.newsFlashFragment -> {
                navController.navigate(R.id.newsFlashFragment)
            }
            R.id.weatherFragment -> {
                navController.navigate(R.id.weatherFragment)
            }
            R.id.aboutUsFragment -> {
                navController.navigate(R.id.aboutUsFragment)
            }
            R.id.eventsFragment -> {
                navController.navigate(R.id.eventsFragment)
            }
            R.id.liveTvFragment -> {
                navController.navigate(R.id.liveTvFragment)
            }
            R.id.triviaFragment -> {
                navController.navigate(R.id.triviaFragment)
            }
            R.id.socialConnectFragment -> {
                navController.navigate(R.id.socialConnectFragment)
            }

        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private val appBarConfiguration by lazy {
        AppBarConfiguration(
            setOf(
                R.id.listenNowFragment,
                R.id.podcastsFragment,
                R.id.newsFlashFragment,
                R.id.weatherFragment,
                R.id.eventsFragment,
                R.id.liveTvFragment,
                R.id.triviaFragment,
                R.id.aboutUsFragment,
                R.id.socialConnectFragment
            ), drawerLayout
        )
    }

    override fun onNavigateUp(): Boolean {
        val navController = findNavController(R.id.host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()

    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }


}

