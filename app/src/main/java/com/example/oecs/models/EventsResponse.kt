package com.example.oecs.models

/**
 * created by Isaac Folarin
 */
data class EventsResponse(
    val title: String,
    val time: String,
    val date: String,
    val venue: String,
    val organizers: String
)
