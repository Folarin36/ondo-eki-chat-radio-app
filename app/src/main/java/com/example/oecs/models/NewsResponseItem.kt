package com.example.oecs.models

import java.io.Serializable

data class NewsResponseItem(
    val approved: Int,
    val body: String,
    val breaking: String,
    val category: String,
    val `data`: String,
    val headline: String,
    val likes: Int,
    val news_id: String,
    val picture: String,
    val review: Int,
    val sn: Int,
    val submitted: Int,
    val tags: String,
    val time: String,
    val time_approved: String,
    val title: String,
    val trend: Int,
    val type: String,
    val writer: Any
): Serializable