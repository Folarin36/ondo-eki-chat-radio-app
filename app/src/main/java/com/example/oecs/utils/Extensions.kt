package com.example.oecs.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar

/**
 * created by Isaac Folarin
 */

fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.displaySnackBar(message: String?) {
    Snackbar.make(this, message!!, Snackbar.LENGTH_LONG).show()
}