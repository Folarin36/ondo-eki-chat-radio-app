package com.example.oecs.utils

object AppConstants {
    const val  CALL_TIMEOUT = 20L
    const val WRITE_TIMEOUT = 30L

    const val BASE_URL: String = "https://dvcsss.com.ng/oecsserver/api/"
    const val TAG = "Error API"

}